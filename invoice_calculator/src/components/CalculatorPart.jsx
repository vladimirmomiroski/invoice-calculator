import React from 'react'

export default function CalculatorPart({data, redValues, deleteInvoice}) {

  const calcSum = (unitPrice, quantity) => {
    let sum = (unitPrice * quantity) / 100
    const dollars = convertingToDollars(sum)
    return dollars
  }

  const convertingToDollars = (dollars) => {
    dollars = dollars.toLocaleString("en-US", {style:"currency", currency:"USD"});
    return dollars
  }


  const estimatedValues = redValues(data)
  
  const returnBgColor = (idx) => {
       return idx % 2 === 0 && "bg-light" 
  }
  return (
    <div className='border-top'>
        <p className='p-3 text-uppercase text-secondary'>Invoices</p>
        {data.length ? data.map(({id, invoiceName, unitPrice, quantity}, idx) => (
             <div key={id} className={`${returnBgColor(idx)} d-flex justify-content-between align-items-center p-1`}>
               <p className='m-0'>{invoiceName}</p>
               <p className='m-0'>{calcSum(unitPrice, quantity)}</p>
               <div>
               <button onClick={() => deleteInvoice(id)} className='btn btn-danger'>delete</button>
               </div>
             </div>
        )) : <p className="text-info h6">No Invoices</p>}
        <div>
          <button className='btn btn-primary mt-5 rounded-circle'>+</button>
        </div>
        <div className='row border-top mt-4 pt-3'>
          <div className="col-6">Net </div>
          <div className="col-6 text-end">${estimatedValues[0] ? estimatedValues[0] : 0}</div>
          <div className="col-6">At-St. (19%)</div>
          <div className="col-6 text-end">${estimatedValues[1] ? estimatedValues[1] : 0}</div>
          <div className="col-6">Gross</div>
          <div className="col-6 text-end">${estimatedValues[2] ? estimatedValues[2] : 0}</div>
          </div>
    </div>
  )
}
