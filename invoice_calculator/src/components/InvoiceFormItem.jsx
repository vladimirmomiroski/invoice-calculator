import React from 'react'

export default function InvoiceFormItem({item, idx, updateField, deleteItem}) {

  const calculatingSum = () => {
    let dollars = (unitPrice * quantity) / 100
    dollars = dollars.toLocaleString("en-US", {style:"currency", currency:"USD"});
    return dollars
  }

    const {id, unitPrice, quantity} = item;
  return (
    <tr className='bg-white'>
    <td className='py-4'>{idx}</td>
    <td>
      <input name={"productName"} onChange={(e) => updateField(e, id)} type="text" className="w-75" />
    </td>
    <td>
      <input name={"description"} onChange={(e) => updateField(e, id)} type="text" className="w-75" />
    </td>
    <td>
      <input name={"quantity"} col="1"onChange={(e) => updateField(e, id)} type="number" className="w-50" />
    </td>
    <td>
      <input name={"unitPrice"} onChange={(e) => updateField(e, id)} type="number" className="w-50" />
    </td>
    <td>{calculatingSum()}</td>
    <td>
      <button className="btn btn-danger" onClick={(e) => deleteItem(e)}>delete</button>
    </td>
  </tr>
  )
}
