import React, { useEffect, useState } from "react";
import InvoiceFormItem from "./InvoiceFormItem";


export default function InvoiceForm({updateMainData, redValues}) {

  const [invoices, setInvoices] = useState([])
  const [customerNum, setCustomerNum] = useState("");
  const [invoiceName, setInvoiceName] = useState("");

  const generalDataHandler = () => {
    const id = new Date().valueOf();
    const invoiceObj = {
      id,
      invoiceName,
      customerNum,
      productName: "",
      description: "",
      quantity: 0,
      unitPrice: 0,
    };
    if (customerNum && invoiceName) {
      setInvoices([...invoices, invoiceObj]);
      setCustomerNum("")
      setInvoiceName("")
    }
  };

  // Delete item from the table
  const deleteItem = (id) => {
       const filteredArr = invoices.filter(el => el.id !== id)
       setInvoices(filteredArr)
  }


  // Input on change to update specific property
  const updateField = (e, id) => {
    const property = e.target.name;
    const value = e.target.value;
    const mapped = invoices.map(el => {
      if(el.id === id) {
        return {...el, [property]: value}
      } else {
        return el
      }
  })
  setInvoices(mapped)
}

// Checking if every input on every row is filled before tranfering the data to main component
const mainDataHandler = () => {
  if(invoices.length) {
      const checkEveryInputVal = invoices.every(({invoiceName, customerNum, productName, description, unitPrice}) => {
         if(invoiceName, customerNum, productName, description, unitPrice) {
           return true;
         }
      })
       if(checkEveryInputVal) {
         updateMainData(invoices) 
         setInvoices([])
       } else {
         alert("You need to fill all inputs")
       }
  } else {
    alert("You don't have any items to save")
  }
  console.log(invoices)
  
}

   const values = redValues(invoices)

  return (
    <div className="pb-4">
      <div className="row text-secondary p-5 mx-auto">
        <div className="col-12 mb-4">
          <p className="text-uppercase">general data</p>
        </div>
        <div className="col-6">
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="customer">Customer Number</label>
            <input
              onChange={(e) => setCustomerNum(e.target.value)}
              id="customer"
              type="number"
              value={customerNum}
              className="w-75 mx-3 py-1"
            />
          </div>
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="name">Name</label>
            <input
              onChange={(e) => setInvoiceName(e.target.value)}
              id="name"
              type="text"
              value={invoiceName}
              className="w-75 mx-3 py-1"
            />
          </div>
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="name">Contact person</label>
            <input
              onChange={(e) => setInvoiceName(e.target.value)}
              id="name"
              type="text"
              className="w-75 mx-3 py-1"
            />
          </div>
        </div>
        <div className="col-6">
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="adress">Adress</label>
            <input id="adress" type="number" className="w-75 mx-3 py-1" />
          </div>
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="zip">ZIP</label>
            <input id="zip" type="number" className="w-75 mx-3 py-1" />
          </div>
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="location">Location</label>
            <input id="location" type="number" className="w-75 mx-3 py-1" />
          </div>
        </div>
        <div className="col-12 my-4">
          <p className="text-uppercase">invoice data</p>
        </div>
        <div className="col-6">
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="invoice_no">Invoice No.</label>
            <input id="invoice_no" type="number" className="w-75 mx-3 py-1" />
          </div>
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="period">Period</label>
            <input id="period" type="number" className="w-75 mx-3 py-1" />
          </div>
        </div>
        <div className="col-6">
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="invoice_name">Invoice name</label>
            <input id="invoice_name" type="number" className="w-75 mx-3 py-1" />
          </div>
          <div className="d-flex justify-content-between align-items-center mb-4">
            <label htmlFor="date">Due date</label>
            <input id="date" type="number" className="w-75 mx-3 py-1" />
          </div>
        </div>
        <div className="col-12 mt-4">
          <p className="text-uppercase">invoice Lines</p>
        </div>

        {/* Table */}
        <table className="col-12 mt-4">
          <thead className="text-secondary">
            <tr>
              <th>Pos</th>
              <th>Name</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Unit price</th>
              <th>Sum</th>
            </tr>
          </thead>
          <tbody>
          {  invoices.length ?
      invoices.map((item, idx) => (
       <InvoiceFormItem deleteItem={deleteItem} updateField={updateField} key={item.id} item={item} idx={idx}/>
      )) : null}
          </tbody>
        </table>
      </div>
      <div className="d-flex justify-content-between mx-5">
      <div>
      <button className="btn btn-primary rounded-circle" onClick={generalDataHandler}>+</button>
      </div>
      <div className="w-25">
        <div className="d-flex justify-content-between">
          <p>Net</p>
          <p>${values[0] ? values[0] : 0}</p>
          </div>
          <div className="d-flex justify-content-between">
          <p>At-St. (19%)</p>
          <p>${values[1] ? values[1] : 0}</p>
          </div>
          <div className="d-flex justify-content-between">
          <p>Gross</p>
          <p>${values[2] ? values[2] : 0}</p>
          </div>
          </div>
      </div>
      <div className="mx-5">
      <button className="btn btn-primary" onClick={mainDataHandler}>Save</button>
      </div>
    </div>
  );
}
