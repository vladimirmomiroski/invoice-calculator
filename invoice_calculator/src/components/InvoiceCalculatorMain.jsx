import React, {useEffect, useState} from "react";
import CalculatorPart from "./CalculatorPart";
import InvoiceForm from "./InvoiceForm";


export default function InvoiceCalculatorMain() {

  const [mainData, setMainData] = useState([]) 

  const updateMainData = (data) => {
          setMainData([...mainData, ...data])
  }

  const deleteInvoice = (id) => {
    const filtered = mainData.filter(el => el.id !== id)
    setMainData(filtered)
  }

  const reducingValues = (data) => {
    if(data.length) {
      let sum = 0
      data.forEach(({quantity, unitPrice}) => {
        sum += (quantity * unitPrice) / 100
      })
      const gross = (sum * 1.19).toFixed(2)
      const at_st = (gross - sum).toFixed(2)
      return [sum, at_st, gross]
    } else {
      return 0
    }   
  }

 

  return (
    <>
      <div>
        <h4 className="py-3 px-5">Invoice Editor</h4>
      </div>
      <div className="row mt-4 mx-auto">
        <div className="col-2">
        <CalculatorPart data={mainData} redValues={reducingValues} deleteInvoice={deleteInvoice} />
        </div>
        <div className="col-10 bg-light">
        <InvoiceForm updateMainData={updateMainData} redValues={reducingValues}/>
        </div>
      </div>
    </>
  );
}
