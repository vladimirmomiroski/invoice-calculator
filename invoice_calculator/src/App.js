import './App.css';
import InvoiceCalculatorMain from './components/InvoiceCalculatorMain';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App" text="center">
      <InvoiceCalculatorMain/>
    </div>
  );
}

export default App;
